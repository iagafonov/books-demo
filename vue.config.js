const MomentLocalesPlugin = require('moment-locales-webpack-plugin')

module.exports = {
  publicPath: '/books-demo/',
  configureWebpack: {
    plugins: [
      // erases all locales except "en"
      new MomentLocalesPlugin()
    ]
  }
}
