import { key } from 'firebase-key'

import { db, storage } from '../firebase'
import { trimObject, initObject } from '../services/validation'
import { Message } from 'element-ui'

const deserializationRules = {
  title: { required: true, type: 'string' },
  authorName: { required: true, type: 'string' },
  authorSurname: { required: true, type: 'string' },
  pageCount: { required: true, type: 'integer' },
  publisher: { required: false, type: 'string' },
  publicationYear: { required: false, type: 'integer' },
  releaseDate: { required: false, type: 'date' },
  imageId: { required: false, type: 'string' }
}

const serializationRules = {
  title: { type: 'string' },
  authorName: { type: 'string' },
  authorSurname: { type: 'string' },
  pageCount: { type: 'integer' },
  publisher: { type: 'string' },
  publicationYear: { type: 'integer' },
  releaseDate: { type: 'date' },
  imageId: { type: 'string' }
}

function createBook () {
  const book = initObject(deserializationRules)
  book.id = null
  book.imageUrl = null
  book.imageIsLoading = false
  return book
}

function deserializeBook (id, data) {
  const book = trimObject(data, deserializationRules)
  book.id = id
  book.imageUrl = null
  book.imageIsLoading = book.imageId != null
  return book
}

function serializeBook (book) {
  return trimObject(book, serializationRules)
}

const collectionRef = db.collection('books').withConverter({
  toFirestore (book) {
    return serializeBook(book)
  },
  fromFirestore (snapshot, options) {
    return deserializeBook(snapshot.id, snapshot.data(options))
  }
})

const storageRef = storage.ref('books')

export default {
  namespaced: true,
  state: {
    book: undefined,
    bookIsLoading: false,
    bookIsSaving: false,
    bookIsMissing: false,
    bookHasLoadingError: false,

    list: [],
    listIsLoading: false,
    listHasLoadingError: false
  },
  mutations: {
    initBook (state) {
      state.book = createBook()
    },
    setBook (state, book) {
      state.book = book
    },
    setBookField (state, { fieldLabel, value, book = state.book }) {
      book[fieldLabel] = value
    },
    setBookIsLoading (state, isLoading) {
      state.bookIsLoading = !!isLoading
    },
    setBookIsSaving (state, isSaving) {
      state.bookIsSaving = !!isSaving
    },
    setBookIsMissing (state, isMissing) {
      state.bookIsMissing = !!isMissing
    },
    setBookHasLoadingError (state, hasLoadingError) {
      state.bookHasLoadingError = !!hasLoadingError
    },
    removeBookImage (state) {
      state.book.removedImageId = state.book.imageId
      state.book.imageId = null
      state.book.imageUrl = null
    },

    setList (state, list) {
      state.list = list
    },
    removeFromList (state, id) {
      state.list = state.list.filter(book => book.id !== id)
    },
    setListIsLoading (state, isLoading) {
      state.listIsLoading = !!isLoading
    },
    setListHasLoadingError (state, hasLoadingError) {
      state.listHasLoadingError = !!hasLoadingError
    }
  },
  actions: {
    fetchList ({ commit }) {
      commit('setListIsLoading', true)
      commit('setListHasLoadingError', false)

      return collectionRef.get().then(query => {
        const list = query.docs
          .map(doc => doc.data())

        commit('setList', list)
      }).catch(err => {
        commit('setListHasLoadingError', true)
        throw err
      }).finally(() => {
        commit('setListIsLoading', false)
      })
    },
    fetchListImageUrls ({ commit }, list) {
      list
        .filter(book => book.imageId != null && !book.imageUrl)
        .map(book => {
          return storageRef.child(book.imageId)
            .getDownloadURL()
            .then(url => {
              commit('setBookField', { fieldLabel: 'imageUrl', value: url, book })
            })
            .finally(() => {
              commit('setBookField', { fieldLabel: 'imageIsLoading', value: false, book })
            })
        })
    },
    fetchBook ({ commit }, id) {
      commit('setBookIsLoading', true)
      commit('setBookHasLoadingError', false)

      return collectionRef.doc(id).get().then(doc => {
        if (doc.exists) {
          const book = doc.data()

          const getUrlPromise = book.imageId
            ? storageRef.child(book.imageId)
              .getDownloadURL()
              .then(url => {
                book.imageUrl = url
                book.imageIsLoading = false
              })
            : Promise.resolve()

          return getUrlPromise.then(() => {
            commit('setBook', book)
            commit('setBookIsMissing', false)
          })
        } else {
          commit('setBook', undefined)
          commit('setBookIsMissing', true)
        }
      }).catch(err => {
        commit('setBookHasLoadingError', true)
        throw err
      }).finally(() => {
        commit('setBookIsLoading', false)
      })
    },
    saveBook ({ commit, state }) {
      commit('setBookIsSaving', true)

      if (state.book.id) {
        return collectionRef
          .doc(state.book.id)
          .set(state.book)
          .then(() => {
            if (state.book.removedImageId) {
              return storageRef.child(state.book.removedImageId).delete()
            }
          })
          .then(() => {
            Message.success('Book saved')
          })
          .catch(err => {
            Message.error('Error saving the book')
            throw err
          }).finally(() => {
            commit('setBookIsSaving', false)
          })
      } else {
        return collectionRef
          .add(state.book)
          .then(() => {
            Message.success('Book added')
          })
          .catch(err => {
            Message.error('Error adding the book')
            throw err
          }).finally(() => {
            commit('setBookIsSaving', false)
          })
      }
    },
    removeBook ({ commit, state }) {
      commit('setBookIsSaving', true)

      return collectionRef
        .doc(state.book.id)
        .delete()
        .then(() => {
          Message.success('Book removed')
          commit('removeFromList', state.book.id)
        })
        .catch(err => {
          Message.error('Error removing the book')
          throw err
        }).finally(() => {
          commit('setBookIsSaving', false)
        })
    },
    uploadBookImage ({ commit }, file) {
      const imageId = key()
      return storageRef.child(imageId).put(file).then(snapshot => {
        return snapshot.ref.getDownloadURL().then(url => {
          commit('setBookField', { fieldLabel: 'imageId', value: imageId })
          commit('setBookField', { fieldLabel: 'imageUrl', value: url })
        })
      })
    }
  }
}
