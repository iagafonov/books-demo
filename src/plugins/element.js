import Vue from 'vue'
import {
  Alert,
  Button,
  Card,
  Col,
  Container,
  DatePicker,
  Form,
  FormItem,
  Header,
  Input,
  InfiniteScroll,
  Image,
  Loading,
  Main,
  Menu,
  MenuItem,
  Message,
  MessageBox,
  Option,
  Radio,
  RadioButton,
  RadioGroup,
  Select,
  Row,
  Upload
} from 'element-ui'
import lang from 'element-ui/lib/locale/lang/ru-RU'
import locale from 'element-ui/lib/locale'

locale.use(lang)

Vue.use(Alert)
Vue.use(Button)
Vue.use(Card)
Vue.use(Col)
Vue.use(Container)
Vue.use(DatePicker)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Header)
Vue.use(Input)
Vue.use(InfiniteScroll)
Vue.use(Image)
Vue.use(Loading)
Vue.use(Main)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Option)
Vue.use(Radio)
Vue.use(RadioButton)
Vue.use(RadioGroup)
Vue.use(Select)
Vue.use(Row)
Vue.use(Upload)

Vue.prototype.$message = Message
Vue.prototype.$msgbox = MessageBox
Vue.prototype.$alert = MessageBox.alert
Vue.prototype.$confirm = MessageBox.confirm
