import Vue from 'vue'
import moment from 'moment'

export const dateFormat = moment.localeData().longDateFormat('L')
  .replace(/YYYY/g, 'yyyy')
  .replace(/DD/g, 'dd')

export const dateValueFormat = 'yyyy-MM-dd'

Vue.prototype.$dateFormat = dateFormat
Vue.prototype.$dateValueFormat = dateValueFormat
